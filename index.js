const HTTP = require(`http`);

http.createServer((req, res) => {

    if(req.url === "/"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to Booking System")
    } else if(req.url === "/profile"){
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Welcome to your profile")
    } else if(req.url === "/courses"){
        res..writeHead(200, {"Content-Type"; "text/plain"})
        res.end("Here's our courses available")
    } else if(req.url === "/addcourse"){
        res..writeHead(200, {"Content-Type"; "text/plain"})
        res.end("Add a course to our resources")
    } else if(req.url === "/updatecourse"){
        res..writeHead(200, {"Content-Type"; "text/plain"})
        res.end("Update a course to our resources")
    } else if(req.url === "/archivecourses"){
        res..writeHead(200, {"Content-Type"; "text/plain"})
        res.end("Archive courses to our resources")}


}).listen(4000)